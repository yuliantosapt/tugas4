<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{asset('fontawesome/css/all.css')}}" rel="stylesheet">
    <title>Hello, world!</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nama</th>
                <th>Posisi</th>
                <th>Perusahaan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{ $employee->nama }}</td>
                <td>
                    {{$employee->position->nama}}
                </td>
                <td>
                    {{$employee->company->nama}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>