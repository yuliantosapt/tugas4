<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <div class="container mt-5 p-2 rounded bg-primary">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Crud Employe</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="employe" class="card-link">Employe</a>
            </div>
        </div>
    </div>
    <div class="container mt-5 p-2 rounded bg-primary">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Crud Company</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="company" class="card-link">Company</a>
            </div>
        </div>
    </div>
</body>

</html>