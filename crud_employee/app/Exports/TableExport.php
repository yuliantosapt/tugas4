<?php

namespace App\Exports;

use App\Models\Employe;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TableExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //    return view('home', ['data' => Employe::all()]);
    // }
    public function view(): View
    {
        return view('cek', [
            'data' => Employe::all()
        ]);
    }
}
