<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    use HasFactory;

    protected $table = 'employees';

    protected $guarded = ['id'];
    public $timestamps = false;
    
    public function position(){
        return $this->belongsTo(position::class, 'atasan_id', 'id');
    }

    public function company(){
        return $this->belongsTo(company::class, 'company_id', 'id');
    }
}
